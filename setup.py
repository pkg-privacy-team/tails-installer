#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from distutils.core import setup, Command
import os
import subprocess
import sys

VERSION = '3.11.8'
LOCALE_DIR= '/usr/share/locale'

if sys.platform == 'win32':
    # FIXME: this is not tested since sooo long...
    locales = []
    if os.path.exists('po/locale'):
        for lang in os.listdir('po/locale'):
            locales.append(os.path.join(lang, 'LC_MESSAGES'))
    # win32com.shell fix from http://www.py2exe.org/index.cgi/win32com.shell
    # ModuleFinder can't handle runtime changes to __path__, but win32com uses them
    try:
        # py2exe 0.6.4 introduced a replacement modulefinder.
        # This means we have to add package paths there, not to the built-in
        # one.  If this new modulefinder gets integrated into Python, then
        # we might be able to revert this some day.
        # if this doesn't work, try import modulefinder
        try:
            import py2exe.mf as modulefinder
        except ImportError:
            import modulefinder
        import win32com
        for p in win32com.__path__[1:]:
            modulefinder.AddPackagePath("win32com", p)
        for extra in ["win32com.shell"]: #,"win32com.mapi"
            __import__(extra)
            m = sys.modules[extra]
            for p in m.__path__[1:]:
                modulefinder.AddPackagePath(extra, p)
    except ImportError:
        # no build path setup, no worries.
        pass

    import py2exe
    LOCALE_DIR = 'locale'

    setup(
        name = 'tails-installer',
        version = VERSION,
        packages = ['tails_installer'],
        scripts = ['tails-installer'], 
        license = 'GNU General Public License (GPL)',
        url = 'https://tails.boum.org/tails-installer',
        description = 'This tool installs a LiveCD ISO on to a USB stick',
        long_description = 'The tails-installer is a cross-platform tool for easily installing live operating systems on to USB flash drives',
        platforms = ['Windows'], 
        maintainer = 'Luke Macken',
        maintainer_email = 'lmacken@redhat.com',
        windows = [
            {
                "script" : "tails-installer",
                # "icon_resources" : [(0, "data/fedora.ico")],
            }
        ],
        options={
            "py2exe" : {
                #"includes" : ["sip", "PyQt4._qt"],
                "includes" : ["sip"],
                'bundle_files': 1,
                # http://stackoverflow.com/questions/1439621/problem-with-loading-win32file-pyd-on-python-2-6
                "dll_excludes": ["mswsock.dll", "MSWSOCK.dll"],
            }
        },
        zipfile=None,
        data_files = [
            "LICENSE.txt",
            ("tools", [
                "tools/dd.exe",
                "tools/syslinux.exe",
                "tools/7z.exe",
                "tools/7z.dll",
                #"tools/7zCon.sfx",
                "tools/7-Zip-License.txt",
            ],)
          ] + [(os.path.join(LOCALE_DIR, locale),
                [os.path.join('po', 'locale', locale, 'tails-installer.mo')])
                for locale in locales]
    )
else:
    from DistUtilsExtra.command import *

    class build_gtkbuilderi18n(Command):
        description = "generate the headers required to use gettext whit gtkbuilder"
        def initialize_options(self):
            pass
        def finalize_options(self):
            pass
        def run(self):
            subprocess.call (["intltool-extract",
                              "--type=gettext/glade",
                              "data/tails-installer.ui"])

    build_extra.build_extra.sub_commands.insert(0, ("build_gtkbuilderi18n", None))

    setup(
        name = 'tails-installer',
        version = VERSION,
        packages = ['tails_installer'],
        scripts = ['tails-installer'],
        license = 'GNU General Public License (GPL)',
        url = 'https://tails.boum.org/contribute/release_process/tails-installer/',
        description = 'This tool installs a LiveCD ISO on to a USB stick',
        long_description = 'The tails-installer is a cross-platform tool for easily installing live operating systems on to USB flash drives',
        platforms = ['Linux'],
        maintainer = 'Tails developers',
        maintainer_email = 'tails@boum.org',
        data_files = [("/usr/share/applications",
                       [ "data/tails-installer.desktop" ]),
                      ("/usr/share/metainfo", ["data/tails-installer.appdata.xml"]),
                      ("/usr/share/pixmaps",["data/tails-installer.svg"]),
                      ("/usr/share/tails-installer", ["data/tails-installer.ui",
                                                      "data/tails-liveusb-header.png"])],
        cmdclass = {
            "build" : build_extra.build_extra,
            "build_gtkbuilderi18n" : build_gtkbuilderi18n,
            "build_i18n" :  build_i18n.build_i18n,
            "build_help" :  build_help.build_help,
            "build_icons" :  build_icons.build_icons,
            "clean": clean_i18n.clean_i18n,
            }
        )

