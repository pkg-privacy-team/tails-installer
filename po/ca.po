# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Albert <provisionalib@hotmail.com>, 2013
# Aleix Vidal i Gaya <aleix.vidal@gmail.com>, 2014
# dartmalak <inactive+dartmalak@transifex.com>, 2014
# Ari Romero <ariadnaaa55555@gmail.com>, 2017
# David Anglada <codiobert@codiobert.es>, 2014
# escufi <escufi@gmail.com>, 2018
# F Xavier Castane <electromigracion@gmail.com>, 2013
# Guillem Arias Fauste <inactive+Mr_SpeedArt@transifex.com>, 2015-2016
# Jordi Mas <jmas@softcatala.org>, 2018
# josep constantí mata <iceberg.jcm@gmail.com>, 2015
# laia_, 2015-2016
# Miquel Bosch, 2018
# Pau Sellés i Garcia <pau.selles@josoc.cat>, 2013
# Vte A.F <viarfer3@inf.upv.es>, 2017
# F Xavier Castane <electromigracion@gmail.com>, 2013
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-23 13:30+0100\n"
"PO-Revision-Date: 2018-09-11 21:03+0000\n"
"Last-Translator: Jordi Mas <jmas@softcatala.org>\n"
"Language-Team: Catalan (http://www.transifex.com/otf/torproject/language/"
"ca/)\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../tails_installer/creator.py:100
msgid "You must run this application as root"
msgstr "Heu d'executar aquesta aplicació amb accés complet"

#: ../tails_installer/creator.py:146
msgid "Extracting live image to the target device..."
msgstr "Extraient la imatge live al dispositiu de destí..."

#: ../tails_installer/creator.py:153
#, python-format
msgid "Wrote to device at %(speed)d MB/sec"
msgstr "S'ha escrit al dispositiu a %(speed)d MB/seg"

#: ../tails_installer/creator.py:296
#, python-format
msgid ""
"There was a problem executing the following command: `%(command)s`.\n"
"A more detailed error log has been written to '%(filename)s'."
msgstr ""
"Ha hagut un problema en executar l'ordre següent: `%(command)s`.\n"
"S'ha desat a '%(filename)s' un registre d'errors més detallat."

#: ../tails_installer/creator.py:315
msgid "Verifying SHA1 checksum of LiveCD image..."
msgstr "Verificant la suma de verificació SHA1 de la imatge LiveCD..."

#: ../tails_installer/creator.py:319
msgid "Verifying SHA256 checksum of LiveCD image..."
msgstr "Verificant la suma de verificació SHA256 de l'imatge LiveCD..."

#: ../tails_installer/creator.py:335
msgid ""
"Error: The SHA1 of your Live CD is invalid.  You can run this program with "
"the --noverify argument to bypass this verification check."
msgstr ""
"Error: El SHA1 del vostre LiveCD és invàlid. Executeu l'aplicació amb "
"l'argument --noverify per saltar-vos aquesta comprovació."

#: ../tails_installer/creator.py:341
msgid "Unknown ISO, skipping checksum verification"
msgstr "ISO desconeguda, s'omet la verificació"

#: ../tails_installer/creator.py:353
#, python-format
msgid ""
"Not enough free space on device.\n"
"%dMB ISO + %dMB overlay > %dMB free space"
msgstr ""
"No hi ha prou espai lliure al dispositiu.\n"
"%dMB ISO + %dMB overlay > %dMB d'espai lliure"

#: ../tails_installer/creator.py:360
#, python-format
msgid "Creating %sMB persistent overlay"
msgstr "Creant una capa persistent de %sMB"

#: ../tails_installer/creator.py:421
#, python-format
msgid "Unable to copy %(infile)s to %(outfile)s: %(message)s"
msgstr "No s'ha pogut copiar %(infile)s a %(outfile)s: %(message)s"

#: ../tails_installer/creator.py:435
msgid "Removing existing Live OS"
msgstr "Eliminant el Sistema Operatiu Live existent."

#: ../tails_installer/creator.py:444 ../tails_installer/creator.py:457
#, python-format
msgid "Unable to chmod %(file)s: %(message)s"
msgstr "No s'ha pogut chmod %(file)s: %(message)s"

#: ../tails_installer/creator.py:450
#, python-format
msgid "Unable to remove file from previous LiveOS: %(message)s"
msgstr "No s'ha pogut eliminar l'arxiu del sistema operatu previ: %(message)s"

#: ../tails_installer/creator.py:464
#, python-format
msgid "Unable to remove directory from previous LiveOS: %(message)s"
msgstr ""
"No s'ha pogut eliminar el directori del Sistema Operatiu previ: %(message)s"

#: ../tails_installer/creator.py:512
#, python-format
msgid "Cannot find device %s"
msgstr "No s'ha pogut trobar el dispositiu %s"

#: ../tails_installer/creator.py:713
#, python-format
msgid "Unable to write on %(device)s, skipping."
msgstr "No s'ha pogut escriure a %(device)s, s'omet."

#: ../tails_installer/creator.py:743
#, python-format
msgid ""
"Some partitions of the target device %(device)s are mounted. They will be "
"unmounted before starting the installation process."
msgstr ""
"Algunes particions del dispositiu de destinació %(device)s estan muntades. "
"Es desmuntaran abans de començar el procés d'instal·lació."

#: ../tails_installer/creator.py:786 ../tails_installer/creator.py:1010
msgid "Unknown filesystem.  Your device may need to be reformatted."
msgstr "El sistema de fitxers és desconegut. S'ha de formatar el dispositiu."

#: ../tails_installer/creator.py:789 ../tails_installer/creator.py:1013
#, python-format
msgid "Unsupported filesystem: %s"
msgstr "El sistema de fitxers de %s no està suportat."

#: ../tails_installer/creator.py:807
#, python-format
msgid "Unknown GLib exception while trying to mount device: %(message)s"
msgstr "Error desconegut de GLib mentre s'intentava muntar: %(message)s"

#: ../tails_installer/creator.py:812
#, python-format
msgid "Unable to mount device: %(message)s"
msgstr "No s'ha pogut muntar el dispositiu: %(message)s"

#: ../tails_installer/creator.py:817
msgid "No mount points found"
msgstr "No es troben els punts de muntatge"

#: ../tails_installer/creator.py:828
#, python-format
msgid "Entering unmount_device for '%(device)s'"
msgstr "Entrant a unmount_device de '%(device)s'"

#: ../tails_installer/creator.py:838
#, python-format
msgid "Unmounting mounted filesystems on '%(device)s'"
msgstr "Desmunant el sistema d'arxius muntat a '%(device)s'"

#: ../tails_installer/creator.py:842
#, python-format
msgid "Unmounting '%(udi)s' on '%(device)s'"
msgstr "Desmuntant '%(udi)s' a '%(device)s'"

#: ../tails_installer/creator.py:853
#, python-format
msgid "Mount %s exists after unmounting"
msgstr "El punt de muntatge %s segueix existint després de desmuntar-lo"

#: ../tails_installer/creator.py:866
#, python-format
msgid "Partitioning device %(device)s"
msgstr "Creant particions a %(device)s"

#: ../tails_installer/creator.py:995
#, python-format
msgid "Unsupported device '%(device)s', please report a bug."
msgstr ""
"El dispositiu '%(device)s' no és compatible, si us plau, informeu d'un error."

#: ../tails_installer/creator.py:998
msgid "Trying to continue anyway."
msgstr "Provant de continuar de totes maneres."

#: ../tails_installer/creator.py:1007 ../tails_installer/creator.py:1405
msgid "Verifying filesystem..."
msgstr "Comprovant el sistema de fitxers..."

#: ../tails_installer/creator.py:1031
#, python-format
msgid "Unable to change volume label: %(message)s"
msgstr "No s'ha pogut canviar l'etiqueta del volum: %(message)s"

#: ../tails_installer/creator.py:1037 ../tails_installer/creator.py:1440
msgid "Installing bootloader..."
msgstr "Instal·lant el carregador de l'arrencada… "

#: ../tails_installer/creator.py:1064
#, python-format
msgid "Could not find the '%s' COM32 module"
msgstr "No s'ha pogut trobar el mòdul COM32 '%s'"

#: ../tails_installer/creator.py:1072 ../tails_installer/creator.py:1458
#, python-format
msgid "Removing %(file)s"
msgstr "Suprimint %(file)s"

#: ../tails_installer/creator.py:1186
#, python-format
msgid "%s already bootable"
msgstr "%s ja pot arrencar"

#: ../tails_installer/creator.py:1206
msgid "Unable to find partition"
msgstr "No s'ha pogut trobar la partició"

#: ../tails_installer/creator.py:1229
#, python-format
msgid "Formatting %(device)s as FAT32"
msgstr "Formatant %(device)s a FAT32"

#: ../tails_installer/creator.py:1289
msgid "Could not find syslinux' gptmbr.bin"
msgstr "No s'ha pogut trobar syslinux' gptmbr.bin"

#: ../tails_installer/creator.py:1302
#, python-format
msgid "Reading extracted MBR from %s"
msgstr "Llegint el MBR extret de %s"

#: ../tails_installer/creator.py:1306
#, python-format
msgid "Could not read the extracted MBR from %(path)s"
msgstr "No s'ha pogut llegir el MBR extret de %(path)s"

#: ../tails_installer/creator.py:1319 ../tails_installer/creator.py:1320
#, python-format
msgid "Resetting Master Boot Record of %s"
msgstr "Reconfigurant el Master Boot Record de %s"

#: ../tails_installer/creator.py:1325
msgid "Drive is a loopback, skipping MBR reset"
msgstr ""
"La unitat és de tipus virtual de repetició, s'omet el reinici del MBR..."

#: ../tails_installer/creator.py:1329 ../tails_installer/creator.py:1589
#, python-format
msgid "Calculating the SHA1 of %s"
msgstr "Calculant la suma de verificació SHA1 de %s"

#: ../tails_installer/creator.py:1354
msgid "Synchronizing data on disk..."
msgstr "Sincronitzant les dades del disc..."

#: ../tails_installer/creator.py:1397
msgid "Error probing device"
msgstr "Error en verificar el dispositiu"

#: ../tails_installer/creator.py:1399
msgid "Unable to find any supported device"
msgstr "No s'ha pogut trobar cap dispositiu compatible"

#: ../tails_installer/creator.py:1409
msgid ""
"Make sure your USB key is plugged in and formatted with the FAT filesystem"
msgstr ""
"Assegureu-vos que la vostra clau USB estigui connectada i que tingui un "
"sistema de fitxers FAT"

#: ../tails_installer/creator.py:1412
#, python-format
msgid ""
"Unsupported filesystem: %s\n"
"Please backup and format your USB key with the FAT filesystem."
msgstr ""
"El sistema de fitxers %s no està suportat\n"
"Deseu la informació i formateu la memòria USB amb un sistema de fitxers FAT."

#: ../tails_installer/creator.py:1481
msgid ""
"Unable to get Win32_LogicalDisk; win32com query did not return any results"
msgstr ""
"No s'ha pogut obtenir el Win32_LogicalDisk; la consulta win32com no ha donat "
"cap resultat"

#: ../tails_installer/creator.py:1536
msgid "Cannot find"
msgstr "No s'ha pogut trobar"

#: ../tails_installer/creator.py:1537
msgid ""
"Make sure to extract the entire tails-installer zip file before running this "
"program."
msgstr ""
"Assegureu-se d'extraure completament el fitxer en zip tails-installer abans "
"d'executar el programa."

#: ../tails_installer/gui.py:69
#, python-format
msgid "Unknown release: %s"
msgstr "Versió desconeguda: %s"

#: ../tails_installer/gui.py:73
#, python-format
msgid "Downloading %s..."
msgstr "Baixant %s..."

#: ../tails_installer/gui.py:213
msgid ""
"Error: Cannot set the label or obtain the UUID of your device.  Unable to "
"continue."
msgstr ""
"Error: no s'ha pogut etiquetar o obtenir l'UUID del vostre dispositiu. No es "
"pot de continuar."

#: ../tails_installer/gui.py:260
#, python-format
msgid "Installation complete! (%s)"
msgstr "Instal·lació complerta! (%s)"

#: ../tails_installer/gui.py:265
msgid "Tails installation failed!"
msgstr "L'instal·lació de Tails ha fracassat!"

#: ../tails_installer/gui.py:369
msgid ""
"Warning: This tool needs to be run as an Administrator. To do this, right "
"click on the icon and open the Properties. Under the Compatibility tab, "
"check the \"Run this program as an administrator\" box."
msgstr ""
"Atenció: Aquesta eina necessita ser executada com Administrador. Per fer-ho, "
"premeu el botó dret a la icona i obriu Propietats. Sota la pestanya de "
"Compatibilitat, activeu la casella \"Executa aquest programa com "
"administrador\"."

#: ../tails_installer/gui.py:381
msgid "Tails Installer"
msgstr "Instal·lador de Tails"

#: ../tails_installer/gui.py:441
#, fuzzy
msgid "Tails Installer is deprecated in Debian"
msgstr "Instal·lador de Tails"

#: ../tails_installer/gui.py:443
msgid ""
"To install Tails from scratch, use GNOME Disks instead.\n"
"<a href='https://tails.boum.org/install/linux/usb-overview'>See the "
"installation instructions</a>\n"
"\n"
"To upgrade Tails, do an automatic upgrade from Tails or a manual upgrade "
"from Debian using a second USB stick.\n"
"<a href='https://tails.boum.org/upgrade/tails-overview'>See the manual "
"upgrade instructions</a>"
msgstr ""

#: ../tails_installer/gui.py:450 ../data/tails-installer.ui.h:2
msgid "Clone the current Tails"
msgstr "Clonar el Tails actual."

#: ../tails_installer/gui.py:457 ../data/tails-installer.ui.h:3
msgid "Use a downloaded Tails ISO image"
msgstr "Usar una imatge d' ISO de Tails descarregada"

#: ../tails_installer/gui.py:494 ../tails_installer/gui.py:815
msgid "Upgrade"
msgstr "Actualització"

#: ../tails_installer/gui.py:496
msgid "Manual Upgrade Instructions"
msgstr "Instruccions d'actualització manual"

#: ../tails_installer/gui.py:498
msgid "https://tails.boum.org/upgrade/"
msgstr "https://tails.boum.org/upgrade/"

#: ../tails_installer/gui.py:506 ../tails_installer/gui.py:727
#: ../tails_installer/gui.py:792 ../data/tails-installer.ui.h:7
msgid "Install"
msgstr "Instal·lar"

#: ../tails_installer/gui.py:509 ../data/tails-installer.ui.h:1
msgid "Installation Instructions"
msgstr "Instruccions d' instal·lació"

#: ../tails_installer/gui.py:511
msgid "https://tails.boum.org/install/"
msgstr "https://tails.boum.org/install/"

#: ../tails_installer/gui.py:517
#, python-format
msgid "%(size)s %(vendor)s %(model)s device (%(device)s)"
msgstr "Dispositiu %(size)s%(vendor)s%(model)s (%(device)s)"

#: ../tails_installer/gui.py:529
msgid "No ISO image selected"
msgstr "No hi ha seleccionada cap imatge ISO"

#: ../tails_installer/gui.py:530
msgid "Please select a Tails ISO image."
msgstr "Per favor, seleccioneu una imatge ISO de Tails."

#: ../tails_installer/gui.py:572
msgid "No device suitable to install Tails could be found"
msgstr "No s'ha pogut trobar un dispositiu compatible per a instal·lar Tails."

#: ../tails_installer/gui.py:574
#, python-format
msgid "Please plug a USB flash drive or SD card of at least %0.1f GB."
msgstr ""
"Per favor, connecteu una memòria USB o una targeta SD d'almenys %0.1f GB."

#: ../tails_installer/gui.py:608
#, python-format
msgid ""
"The USB stick \"%(pretty_name)s\" is configured as non-removable by its "
"manufacturer and Tails will fail to start on it. Please try installing on a "
"different model."
msgstr ""
"La memòria USB \"%(pretty_name)s\" està configurada com a no extraïble pel "
"propi fabricant i Tails no podrà executar-se. Per favor, intenteu instal·lar-"
"lo en un model diferent."

#: ../tails_installer/gui.py:618
#, python-format
msgid ""
"The device \"%(pretty_name)s\" is too small to install Tails (at least "
"%(size)s GB is required)."
msgstr ""
"El dispositiu \"%(pretty_name)s\" és massa petit per instal·lar-hi el Tails "
"(es necessiten com  mínim %(size)s GB)."

#: ../tails_installer/gui.py:631
#, python-format
msgid ""
"To upgrade device \"%(pretty_name)s\" from this Tails, you need to use a "
"downloaded Tails ISO image:\n"
"https://tails.boum.org/install/download"
msgstr ""
"Per actualitzar el dispositiu \"%(pretty_name)s\" d'aquest Tails, necessiteu "
"utilitzar una imatge ISO Tails descarregada:\n"
"https://tails.boum.org/install/download"

#: ../tails_installer/gui.py:652
msgid "An error happened while installing Tails"
msgstr "Ha ocorregut un error al instalar Tails."

#: ../tails_installer/gui.py:664
msgid "Refreshing releases..."
msgstr "Cercant noves versions..."

#: ../tails_installer/gui.py:669
msgid "Releases updated!"
msgstr "Noves versions actualitzades!"

#: ../tails_installer/gui.py:722
msgid "Installation complete!"
msgstr "Instal·lació complerta!"

#: ../tails_installer/gui.py:738
msgid "Cancel"
msgstr ""

#: ../tails_installer/gui.py:774
msgid "Unable to mount device"
msgstr "No s'ha pogut muntar el dispositiu."

#: ../tails_installer/gui.py:781 ../tails_installer/gui.py:814
msgid "Confirm the target USB stick"
msgstr "Confirma el llapis USB de destinació"

#: ../tails_installer/gui.py:782
#, python-format
msgid ""
"%(size)s %(vendor)s %(model)s device (%(device)s)\n"
"\n"
"All data on this USB stick will be lost."
msgstr ""
"Dispositiu %(size)s%(vendor)s%(model)s (%(device)s)\n"
"\n"
"Tota l'informació en aquesta memòria USB es perdrà."

#: ../tails_installer/gui.py:801
#, python-format
msgid "%(parent_size)s %(vendor)s %(model)s device (%(device)s)"
msgstr "Dispositiu %(parent_size)s%(vendor)s%(model)s(%(device)s)"

#: ../tails_installer/gui.py:809
msgid ""
"\n"
"\n"
"The persistent storage on this USB stick will be preserved."
msgstr ""
"\n"
"\n"
"Es conservarà l'emmagatzematge persistent d'aquesta memòria USB."

#: ../tails_installer/gui.py:810
#, python-format
msgid "%(description)s%(persistence_message)s"
msgstr "%(description)s%(persistence_message)s"

#: ../tails_installer/gui.py:853
msgid "Download complete!"
msgstr "Baixada completa!"

#: ../tails_installer/gui.py:857
msgid "Download failed: "
msgstr "Ha fallat la baixada:"

#: ../tails_installer/gui.py:858
msgid "You can try again to resume your download"
msgstr "Podeu tornar-ho a intentar per continuar la vostra baixada"

#: ../tails_installer/gui.py:866
msgid ""
"The selected file is unreadable. Please fix its permissions or select "
"another file."
msgstr ""
"El fitxer seleccionat no s'ha pogut llegir. Ajusteu els permisos o "
"seleccioneu un altre fitxer."

#: ../tails_installer/gui.py:872
msgid ""
"Unable to use the selected file.  You may have better luck if you move your "
"ISO to the root of your drive (ie: C:\\)"
msgstr ""
"No s'ha pogut fer servir l'arxiu seleccionat. Moveu l'ISO a l'arrel de la "
"unitat (per exemple: C:\\)"

#: ../tails_installer/gui.py:878
#, python-format
msgid "%(filename)s selected"
msgstr "%(filename)s seleccionat"

#: ../tails_installer/source.py:28
msgid "Unable to find LiveOS on ISO"
msgstr "No es pot trobar LiveOS en ISO"

#: ../tails_installer/source.py:34
#, python-format
msgid "Could not guess underlying block device: %s"
msgstr "No s'ha pogut trobar el dispositiu orientat a blocs: %s"

#: ../tails_installer/source.py:49
#, python-format
msgid ""
"There was a problem executing `%s`.\n"
"%s\n"
"%s"
msgstr ""
"Ha ocorregut un problema executant `%s`.\n"
"%s\n"
"%s"

#: ../tails_installer/source.py:63
#, python-format
msgid "'%s' does not exist"
msgstr "'%s' no existeix."

#: ../tails_installer/source.py:65
#, python-format
msgid "'%s' is not a directory"
msgstr "'%s' no és un directori."

#: ../tails_installer/source.py:75
#, python-format
msgid "Skipping '%(filename)s'"
msgstr "Ometent '%(filename)s'"

#: ../tails_installer/utils.py:44
#, python-format
msgid ""
"There was a problem executing `%s`.%s\n"
"%s"
msgstr ""
"Ha ocorregut un problema executant `%s`.%s\n"
"%s"

#: ../tails_installer/utils.py:124
msgid "Could not open device for writing."
msgstr "No s'ha pogut obrir el dispositiu per a l'escriptura."

#: ../data/tails-installer.ui.h:4
msgid "Select a distribution to download:"
msgstr "Seleccioneu una distribució per a descarregar:"

#: ../data/tails-installer.ui.h:5
msgid "Target USB stick:"
msgstr "Memòria USB de destí:"

#: ../data/tails-installer.ui.h:6
msgid "Reinstall (delete all data)"
msgstr "Reinstal·lar (esborrar tots els arxius)"
